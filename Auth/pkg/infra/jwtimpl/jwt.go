package jwtimpl

import (
	"errors"
	"fmt"
	"time"

	jwt4 "github.com/golang-jwt/jwt/v4"
)

func CreateTokenJwt(subject string, expirationTime time.Time, secret string) (string, error) {
	claims := jwt4.RegisteredClaims{
		Issuer:    "Auth",
		ExpiresAt: jwt4.NewNumericDate(expirationTime),
		Subject:   subject,
	}
	token := jwt4.NewWithClaims(jwt4.SigningMethodHS256, claims)
	tokenString, err := token.SignedString([]byte(secret))
	return tokenString, err
}

func ParseTokenJwt(tokenString string, secret string) (*jwt4.Token, error) {
	keyFunc := func(t *jwt4.Token) (interface{}, error) {
		if t.Method.Alg() != jwt4.SigningMethodHS256.Alg() {
			return nil, fmt.Errorf("unexpected jwt signing method=%v", t.Header["alg"])
		}

		return []byte(secret), nil
	}
	token, err := jwt4.Parse(tokenString, keyFunc)
	if err != nil {
		if errors.Is(err, jwt4.ErrTokenExpired) {
			return token, err
		}
		return nil, err
	}
	if !token.Valid {
		return nil, errors.New("invalid token")
	}
	return token, nil
}
