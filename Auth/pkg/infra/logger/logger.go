package logger

import (
	"fmt"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

type Logger = *zap.Logger

func New() (Logger, error) {
	zapCfg := zap.NewDevelopmentConfig()
	l, err := zapCfg.Build()
	if err != nil {
		return nil, fmt.Errorf("create logger failed: %w", err)
	}
	zap.ReplaceGlobals(l)
	return l, nil
}

func CookieMiddleWare(log Logger) func(ctx *gin.Context) {
	return func(ctx *gin.Context) {
		coockies := ctx.Request.Cookies()
		var coockiesStr string
		for i := range coockies {
			coockiesStr += "(" + coockies[i].Name + "=" + coockies[i].Value + ") "
		}
		log.With(
			zap.String("coockies", coockiesStr),
		).Info("Coockies check")
		ctx.Next()
	}
}
