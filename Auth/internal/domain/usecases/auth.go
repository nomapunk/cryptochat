package usecases

import (
	"auth/internal/domain/models"
	"auth/internal/ports"
	"fmt"

	"github.com/caarlos0/env"
)

type Auth struct {
	cfg *Config

	storage ports.Store
}

type Config struct {
}

func (auth *Auth) Login(login string, password string) error {
	_, err := auth.storage.GetUser(login, password)
	if err != nil {
		return err
	}
	return nil
}

func (auth *Auth) Auth() error {
	return nil
}

func (auth *Auth) Register(login string, password string) error {
	err := auth.storage.InsertUser(models.NewUser(login, password))
	if err != nil {
		return err
	}
	return nil
}

func (c *Config) Parse() error {
	if err := env.Parse(c); err != nil {
		return err
	}
	return nil
}

func New(storage ports.Store) (*Auth, error) {
	var cfg Config
	if err := env.Parse(&cfg); err != nil {
		return nil, fmt.Errorf("parse domain config failed: %w", err)
	}
	return &Auth{
		storage: storage,
	}, nil
}
