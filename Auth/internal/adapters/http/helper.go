package http

import (
	"auth/pkg/infra/jwtimpl"
	"fmt"

	"github.com/gin-gonic/gin"
	jwt4 "github.com/golang-jwt/jwt/v4"
)

func (a *Adapter) GetJwtTokens(ctx *gin.Context) (*jwt4.Token, *jwt4.Token, error) {
	accessTokenRaw, okAccess := ctx.Cookie("access_token")
	refreshTokenRaw, okRefresh := ctx.Cookie("refresh_token")
	if okAccess != nil || okRefresh != nil {
		return nil, nil, fmt.Errorf("jwt-tokens not found: make login")
	}
	accessToken, _ := jwtimpl.ParseTokenJwt(accessTokenRaw, a.cfg.JwtSecret)
	refreshToken, _ := jwtimpl.ParseTokenJwt(refreshTokenRaw, a.cfg.JwtSecret)
	if accessToken == nil {
		return nil, nil, fmt.Errorf("jwt-tokens broken: make login")
	}
	if refreshToken == nil {
		return nil, nil, fmt.Errorf("jwt-tokens broken: make login")
	}
	return accessToken, refreshToken, nil
}
