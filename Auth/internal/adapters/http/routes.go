package http

import (
	"auth/pkg/infra/logger"
	"net/http"
	"time"

	ginzap "github.com/gin-contrib/zap"
	"github.com/gin-gonic/gin"
	"github.com/juju/zaputil/zapctx"
)

func initRouter(a *Adapter, r *gin.Engine) {
	r.Use(func(ctx *gin.Context) {
		lCtx := zapctx.WithLogger(ctx.Request.Context(), a.log)
		ctx.Request = ctx.Request.WithContext(lCtx)
	})
	r.Use(func(ctx *gin.Context) {
		ctx.Header("Access-Control-Allow-Origin", "http://localhost:3000")
		ctx.Header("Access-Control-Allow-Methods", "*")
		ctx.Header("Access-Control-Allow-Headers", "Authorization, Content-Type, Date, Content-Length")
		ctx.Header("Access-Control-Allow-Credentials", "true")
		if ctx.Request.Method == http.MethodOptions {
			ctx.Status(http.StatusOK)
			return
		}
		ctx.Next()
	})
	r.Use(logger.CookieMiddleWare(a.log))
	r.Use(ginzap.Ginzap(a.log, time.RFC3339, true))
	r.Use(ginzap.RecoveryWithZap(a.log, true))
	api := r.Group("/cryptochat/auth/api/v1")
	{
		api.GET("auth", a.getAuth)
		api.GET("login", a.getLogin)
		api.POST("register", a.postRegister)
	}
}
