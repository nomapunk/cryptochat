package http

import (
	"auth/pkg/infra/jwtimpl"
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	jwt4 "github.com/golang-jwt/jwt/v4"
)

func (a *Adapter) getLogin(ctx *gin.Context) {
	login, password, ok := ctx.Request.BasicAuth()
	if !ok {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"text": "Basic-Auth expected",
		})
		return
	}
	err := a.auth.Login(login, password)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"text": err.Error(),
		})
		return
	}
	accessToken, err := jwtimpl.CreateTokenJwt(login, time.Now().Add(time.Minute), a.cfg.JwtSecret)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"text": err.Error(),
		})
		return
	}
	refreshToken, err := jwtimpl.CreateTokenJwt(login, time.Now().Add(time.Hour), a.cfg.JwtSecret)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"text": err.Error(),
		})
		return
	}
	http.SetCookie(ctx.Writer, &http.Cookie{Name: "access_token", Value: accessToken, Expires: time.Now().Add(time.Hour * 24), Path: "/"})
	http.SetCookie(ctx.Writer, &http.Cookie{Name: "refresh_token", Value: refreshToken, Expires: time.Now().Add(time.Hour * 24), Path: "/"})
	ctx.JSON(http.StatusOK, gin.H{
		"text": fmt.Sprintf("Hello, %s", login),
	})
}

func (a *Adapter) getAuth(ctx *gin.Context) {
	accessToken, refreshToken, err := a.GetJwtTokens(ctx)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"text": err.Error(),
		})
		return
	}
	err = a.auth.Auth()
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"text": err.Error(),
		})
		return
	}
	login := accessToken.Claims.(jwt4.MapClaims)["sub"].(string)
	if accessToken != nil {
		ctx.JSON(http.StatusOK, gin.H{
			"text": fmt.Sprintf("Hello, %s", login),
		})
		http.SetCookie(ctx.Writer, &http.Cookie{Name: "access_token", Value: accessToken.Raw, Expires: time.Now().Add(time.Hour * 24), Path: "/"})
		http.SetCookie(ctx.Writer, &http.Cookie{Name: "refresh_token", Value: refreshToken.Raw, Expires: time.Now().Add(time.Hour * 24), Path: "/"})
	} else if refreshToken != nil {
		accessTokenNew, err := jwtimpl.CreateTokenJwt(login, time.Now().Add(time.Minute), a.cfg.JwtSecret)
		if err != nil {
			ctx.JSON(http.StatusForbidden, gin.H{
				"text": err.Error(),
			})
			return
		}
		refreshTokenNew, err := jwtimpl.CreateTokenJwt(login, time.Now().Add(time.Hour), a.cfg.JwtSecret)
		if err != nil {
			ctx.JSON(http.StatusForbidden, gin.H{
				"text": err.Error(),
			})
			return
		}
		http.SetCookie(ctx.Writer, &http.Cookie{Name: "access_token", Value: accessTokenNew, Expires: time.Now().Add(time.Hour * 24), Path: "/"})
		http.SetCookie(ctx.Writer, &http.Cookie{Name: "refresh_token", Value: refreshTokenNew, Expires: time.Now().Add(time.Hour * 24), Path: "/"})
		ctx.JSON(http.StatusOK, gin.H{
			"text": fmt.Sprintf("Hello, %s", login),
		})
	} else {
		ctx.JSON(http.StatusForbidden, gin.H{
			"text": "make login",
		})
	}
}

func (a *Adapter) postRegister(ctx *gin.Context) {
	login, password, ok := ctx.Request.BasicAuth()
	if !ok {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"text": "Basic-Auth header expected",
		})
		return
	}
	err := a.auth.Register(login, password)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"text": err.Error(),
		})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"text": fmt.Sprintf("Success register, %s! :)", login),
	})
}
