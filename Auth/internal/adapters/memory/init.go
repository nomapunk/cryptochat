package memory

import (
	"auth/internal/domain/models"
	"auth/pkg/infra/logger"
)

type MemoryStorage struct {
	users map[string]*models.User
}

func New(l logger.Logger) (*MemoryStorage, error) {
	users := make(map[string]*models.User)
	return &MemoryStorage{
		users: users,
	}, nil
}
