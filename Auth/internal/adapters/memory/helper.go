package memory

import (
	"auth/internal/domain/models"
	"auth/internal/ports"
	"fmt"
)

func (s *MemoryStorage) FindUser(login string) (bool, error) {
	_, ok := s.users[login]
	return ok, nil
}

func (s *MemoryStorage) GetUser(login string, password string) (ports.User, error) {
	if ok, _ := s.FindUser(login); !ok {
		return nil, fmt.Errorf("GetUser: user not found")
	}
	user := s.users[login]
	if password != user.GetPassword() {
		return nil, fmt.Errorf("GetUser: invalid password")
	}
	return user, nil
}

func (s *MemoryStorage) InsertUser(user ports.User) error {
	if ok, _ := s.FindUser(user.GetLogin()); ok {
		return fmt.Errorf("InsertUser: user arleady pushed")
	}
	s.users[user.GetLogin()] = models.NewUser(user.GetLogin(), user.GetPassword())
	return nil
}
