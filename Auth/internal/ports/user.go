package ports

type User interface {
	GetLogin() string
	GetPassword() string
}
