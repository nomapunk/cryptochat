import './App.css';
import { React } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from './Components/Header';
import {useState} from 'react'
import Home from './Pages/Home';
import Login from './Pages/Login';
import Register from './Pages/Register';
import { BrowserRouter as Router, Routes as Switch, Route, renderMatches } from 'react-router-dom';
import axios from 'axios';
export function App() {
  const [auth, setauth] = useState(false);
  async function authInit() {
    let url = "http://localhost:3001/cryptochat/auth/api/v1/auth";
    let response = await axios.get(url, {
        withCredentials: true
    })
    if (response.status == 200) {
        setauth(true)
        console.log(auth, response.status)
    }
    console.log(response.data)
  }
  authInit();
  return (
    <div>
      <Header auth={auth} setauth={setauth}/>
      <Switch>
          <Route path="/" element={<Home auth={auth} setauth={setauth}/>}/>
          <Route path="/login" element={<Login auth={auth} setauth={setauth}/>}/>
          <Route path="/register" element={<Register auth={auth} setauth={setauth}/>}/>
      </Switch>
    </div>
  );
}