import React, { Component } from 'react';
import { Button, Container, FormControl, Nav, Navbar, Form } from 'react-bootstrap';
import { BrowserRouter as Router, Routes as Switch, Route, renderMatches, Link } from 'react-router-dom';
import logo from './logo192.png';
import { useNavigate } from 'react-router';
import Cookies from 'js-cookie';
function Header({auth, setauth}) {
    const linkStyle = {
        margin: "1rem",
        textDecoration: "none",
        color: 'white'
    };
    const nav = useNavigate();
    async function handleLogOut() {
        console.log("Auth: ", auth)
        Cookies.remove('refresh_token')
        Cookies.remove('access_token')
        setauth(false)
        nav("/")
        console.log("Auth: ", auth)
    }
    return (
        <>
            <Navbar collapseOnSelect expand="md" bg="dark" variant="dark">
                <Container>
                    <Link to="/">
                    <img
                        src={logo}
                        height="30"
                        weight="30"
                        className="d-inline-block align-top"
                        alt="Logo"
                    />
                    </Link>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
                    {
                    auth ?
                    <Navbar.Collapse id="responsive-navbar-nav">
                            <Nav className="me-auto p-2">
                                <Button variant="outline-info" onClick={handleLogOut}>Logout</Button>
                            </Nav>
                            <Form className="d-flex p-2">
                                <FormControl type="text" placeholder="Search" className="me-sm-2"/>
                                <Button variant="outline-info">Search</Button>
                            </Form>
                    </Navbar.Collapse>
                    :
                    <Navbar.Collapse id="responsive-navbar-nav">
                            <Nav className="me-auto">
                                <Link to="/login" style={linkStyle}>Login</Link>
                                <Link to="/register" style={linkStyle}>Register</Link>
                            </Nav>
                            <Form className="d-flex p-2">
                                <FormControl type="text" placeholder="Search" className="me-sm-2"/>
                                <Button variant="outline-info">Search</Button>
                            </Form>
                    </Navbar.Collapse>
                    }
                </Container>
            </Navbar>
        </>
    )
}

export default Header;