import React, { Component } from 'react'
import {Card, Button, Form} from 'react-bootstrap'
import {useState} from 'react'
import axios from 'axios';

function ChatCard({id, owner_login, type, text, parent}) {
  const [cards, setcards] = useState([])
  const [isReply, setReply] = useState(false)
  async function addComment() {
    let url = 'http://localhost:3003/cryptochat/publish/api/v1/add_comment';
    let response = await axios.post(url, {}, {
        params: { 
          owner_login: owner_login,
          type: type,
          text: document.getElementById('textForm').value,
          parent: id
        },
        withCredentials: true
    })
    console.log(response.data)
  }
  
  function Reply() {
    setReply(true)
  }

  async function Send() {
    setReply(false)
    addComment()
    console.log(getComments())
    while (await getComments() === false) {
      // wait
    }
  }

  async function getComments() {
    let url = 'http://localhost:3002/cryptochat/comments/api/v1/get_comments';
    let response = await axios.get(url, {
        params: {
          id: id
        },
        withCredentials: true
    })
    let json = JSON.parse(response.data);
    if (json != null && cards != json) {
      setcards(json);
      return true
    }
    return false
  }
  return (
    <div className='p-2'>
      <Card className='d-flex'>
        <Card.Body>
          <Card.Text>
            {text}
          </Card.Text>
          <div className='p-m-2'>
            <Button variant="primary" onClick={Reply}>Reply</Button>
            <Button variant="primary" onClick={getComments}>Show</Button>
          </div>
        {cards.length != 0 ? cards.map(({id, ownerLogin, commentType, text, parent}) => (
          <ChatCard 
          id={id}
          owner_login={ownerLogin}
          type={commentType}
          text={text}
          parent={parent}/>
        )) : <div></div>}
        </Card.Body>
      </Card>
      {
        isReply ? 
        <div className='d-flex'>
          <Form>
            <Form.Label htmlFor="textForm">Text</Form.Label>
            <Form.Control
              type="text"
              id="textForm"
            />
            <Button onClick={Send}>Send</Button>
          </Form>
        </div> 
        : 
        <div></div>
      }
    </div>
  )
}

function Home({auth, setauth}) {
  return (
    <ChatCard text="123" id="-1" type="0" owner_login="sasha"/>
  )
}

export default Home;
