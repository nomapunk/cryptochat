import React, { useState } from 'react';
import { useNavigate } from 'react-router';
import {
  MDBContainer,
  MDBTabs,
  MDBTabsItem,
  MDBTabsLink,
  MDBTabsContent,
  MDBTabsPane,
  MDBBtn,
  MDBIcon,
  MDBInput,
  MDBCheckbox
}
from 'mdb-react-ui-kit';
import axios from 'axios';

function Login({auth, setauth}) {
    const [justifyActive, setJustifyActive] = useState('tab1');
    const nav = useNavigate();
    async function login() {
        let user = document.getElementById('login-form').value;
        let password = document.getElementById('password-form').value;
        var token = user + ":" + password;
        var hash = btoa(token);
        console.log(user, password, token, hash)
        let url = "http://localhost:3001/cryptochat/auth/api/v1/login";
        let response = await axios.get(url, {
            headers: {
                "Authorization": "Basic " + hash
            },
            withCredentials: true
        })
        if (response.status == 200) {
            await setauth(true)
            console.log(auth, response.status)
            nav("/");
        }
        console.log(response.data)
    }
    return (
      <MDBContainer className="p-3 my-5 d-flex flex-column w-50">
  
        <MDBTabsContent>
  
          <MDBTabsPane show={justifyActive === 'tab1'}>
  
            <div className="text-center mb-3">
              <div className='d-flex justify-content-between mx-auto' style={{width: '40%'}}>
                <MDBBtn tag='a' color='none' className='m-1' style={{ color: '#1266f1' }}>
                  <MDBIcon fab icon='facebook-f' size="sm"/>
                </MDBBtn>
  
                <MDBBtn tag='a' color='none' className='m-1' style={{ color: '#1266f1' }}>
                  <MDBIcon fab icon='twitter' size="sm"/>
                </MDBBtn>
  
                <MDBBtn tag='a' color='none' className='m-1' style={{ color: '#1266f1' }}>
                  <MDBIcon fab icon='google' size="sm"/>
                </MDBBtn>
  
                <MDBBtn tag='a' color='none' className='m-1' style={{ color: '#1266f1' }}>
                  <MDBIcon fab icon='github' size="sm"/>
                </MDBBtn>
              </div>
            </div>
  
            <MDBInput wrapperClass='mb-4' label='Login' id='login-form' type='email'/>
            <MDBInput wrapperClass='mb-4' label='Password' id='password-form' type='password'/>
  
            <MDBBtn className="mb-4 w-100" onClick={login} type="button">Login</MDBBtn>
  
          </MDBTabsPane>
        </MDBTabsContent>
  
      </MDBContainer>
    );
}

export default Login;