import React, { useState } from 'react';
import { useNavigate } from 'react-router';
import {
  MDBContainer,
  MDBTabs,
  MDBTabsItem,
  MDBTabsLink,
  MDBTabsContent,
  MDBTabsPane,
  MDBBtn,
  MDBIcon,
  MDBInput,
  MDBCheckbox
}
from 'mdb-react-ui-kit';
import axios from 'axios';

function Register({auth, setauth}) {
    const [justifyActive, setJustifyActive] = useState('tab2');
    const nav = useNavigate();

    async function register() {
        let user = document.getElementById('login-form').value;
        let password = document.getElementById('password-form').value;
        var token = user + ":" + password;
        var hash = btoa(token);
        console.log(user, password, token, hash)
        let url = "http://localhost:3001/cryptochat/auth/api/v1/register";
        let response = await axios.post(url, {}, {
            withCredentials: true,
            headers: {
                "Authorization": "Basic " + hash
            }
        })
        if (response.status == 200) {
            await setauth(true)
            console.log(auth, response.status)
            nav("/");
        }
        console.log(response.data)
    }
  
    return (
      <MDBContainer className="p-3 my-5 d-flex flex-column w-50">
        <MDBTabsContent>  
          <MDBTabsPane show={justifyActive === 'tab2'}>  
            <MDBInput wrapperClass='mb-4' label='Login' id='login-form' type='text'/>
            <MDBInput wrapperClass='mb-4' label='Password' id='password-form' type='password'/>
  
            <MDBBtn className="mb-4 w-100" onClick={register} type="button">Register</MDBBtn>
          </MDBTabsPane>
  
        </MDBTabsContent>
  
      </MDBContainer>
    );
}

export default Register;