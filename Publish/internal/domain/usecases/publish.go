package usecases

import (
	"fmt"
	"publish/internal/domain/models"
	"publish/internal/ports"
	"sync/atomic"

	"github.com/caarlos0/env"
)

type Publish struct {
	cfg     *Config
	id      atomic.Int32
	storage ports.Store
}

type Config struct {
}

func (c *Config) Parse() error {
	if err := env.Parse(c); err != nil {
		return err
	}
	return nil
}

func (publish *Publish) getNextId() int {
	publish.id.Add(1)
	return int(publish.id.Load())
}

func (publish *Publish) AddComment(ownerLogin string, commentType int, text string, parent int) (ports.Comment, error) {
	comment := models.NewComment(publish.getNextId(), ownerLogin, commentType, text, parent)
	return comment, publish.storage.AddComment(comment)
}

func New(storage ports.Store) (*Publish, error) {
	var cfg Config
	if err := env.Parse(&cfg); err != nil {
		return nil, fmt.Errorf("parse domain config failed: %w", err)
	}
	lastId, err := storage.GetLastComment()
	if err != nil {
		return nil, fmt.Errorf("failed get last comment %d: %w", lastId, err)
	}
	var id atomic.Int32
	id.Store(int32(lastId))
	return &Publish{
		storage: storage,
		id:      id,
	}, nil
}
