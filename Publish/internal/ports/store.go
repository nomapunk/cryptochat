package ports

type Store interface {
	AddComment(comment Comment) error
	GetLastComment() (int, error)
}
