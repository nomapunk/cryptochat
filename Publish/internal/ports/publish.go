package ports

type Publish interface {
	AddComment(ownerLogin string, commentType int, text string, parent int) (Comment, error)
}
