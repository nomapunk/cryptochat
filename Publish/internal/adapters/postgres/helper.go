package postgres

import (
	"context"
	"publish/internal/ports"

	"github.com/jackc/pgx/v4"
)

func (s *PostgresStorage) AddComment(comment ports.Comment) error {
	ctx := context.Background()
	db, err := pgx.Connect(context.Background(), s.cfg.Postgres_url)
	if err != nil {
		return err
	}
	defer db.Close(context.Background())
	tx, err := db.Begin(ctx)
	if err != nil {
		return err
	}
	defer tx.Rollback(ctx)
	batch := new(pgx.Batch)
	batch.Queue(
		"INSERT INTO comments (id, owner_login, type, text, parent) VALUES ($1, $2, $3, $4, $5);",
		comment.GetId(),
		comment.GetOwnerLogin(),
		comment.GetType(),
		comment.GetText(),
		comment.GetParent(),
	)
	res := tx.SendBatch(ctx, batch)
	err = res.Close()
	if err != nil {
		return err
	}
	return tx.Commit(ctx)
}

func (s *PostgresStorage) getCountComments() (int, error) {
	db, err := pgx.Connect(context.Background(), s.cfg.Postgres_url)
	if err != nil {
		return 0, err
	}
	defer db.Close(context.Background())
	rows, err := db.Query(context.Background(), "SELECT COUNT(id) FROM comments;")
	if err != nil {
		return 0, err
	}
	cnt := 0
	for rows.Next() {
		err = rows.Scan(
			&cnt,
		)
		if err != nil {
			return 0, err
		}
	}
	return cnt, nil
}

func (s *PostgresStorage) GetLastComment() (int, error) {
	cnt, err := s.getCountComments()
	if err != nil {
		return 0, err
	}
	if cnt == 0 {
		return 0, nil
	}
	db, err := pgx.Connect(context.Background(), s.cfg.Postgres_url)
	if err != nil {
		return 0, err
	}
	defer db.Close(context.Background())
	rows, err := db.Query(context.Background(), "SELECT MAX(id) FROM comments;")
	if err != nil {
		return 0, err
	}
	lastId := 0
	for rows.Next() {
		err = rows.Scan(
			&lastId,
		)
		if err != nil {
			return 0, err
		}
	}
	return lastId, nil
}
