package http

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func (a *Adapter) addComment(ctx *gin.Context) {
	ownerLogin := ctx.Query("owner_login")
	typeCommentRaw := ctx.Query("type")
	text := ctx.Query("text")
	parentRaw := ctx.Query("parent")
	if len(ownerLogin) == 0 || len(typeCommentRaw) == 0 || len(text) == 0 || len(parentRaw) == 0 {
		ctx.JSON(http.StatusBadRequest, &gin.H{
			"text": "must owner_login, type, text, parent in query",
		})
		return
	}
	typeComment, err := strconv.Atoi(typeCommentRaw)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, &gin.H{
			"text": err.Error(),
		})
		return
	}
	parent, err := strconv.Atoi(parentRaw)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, &gin.H{
			"text": err.Error(),
		})
		return
	}
	comment, err := a.publish.AddComment(ownerLogin, typeComment, text, parent)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, &gin.H{
			"text": err.Error(),
		})
		return
	}
	ctx.JSON(http.StatusOK, &gin.H{
		"text": fmt.Sprintf("all right, new id is: %d", comment.GetId()),
	})
}
