package application

import (
	"context"
	"publish/internal/adapters/http"
	"publish/internal/adapters/postgres"
	"publish/internal/domain/usecases"
	"publish/pkg/infra/logger"
	"publish/pkg/infra/probes"
)

type App struct {
	l             logger.Logger
	shutdownFuncs []func(ctx context.Context) error
}

func New(l logger.Logger) *App {
	return &App{
		l: l,
	}
}

func (app *App) Start() error {
	probes, _ := probes.New(app.l)
	probes.SetStarted()
	err := probes.Start()
	if err != nil {
		app.l.Sugar().Fatalf("probes started failed: %w", err)
	}
	/*
		TODO metrics
	*/
	store, err := postgres.New()
	if err != nil {
		app.l.Sugar().Fatalf("store started failed: %w", err)
	}
	app.shutdownFuncs = append(app.shutdownFuncs, store.Stop)
	publish, err := usecases.New(store)
	if err != nil {
		app.l.Sugar().Fatalf("create business logic failed: %w", err)
	}
	server, err := http.New(publish, app.l)
	app.shutdownFuncs = append(app.shutdownFuncs, server.Stop)
	err = server.Start()
	if err != nil {
		app.l.Sugar().Fatalf("server not started: %w", err)
	}
	probes.SetReady()
	return nil
}

func (a *App) Stop(ctx context.Context) error {
	var err error
	for i := len(a.shutdownFuncs) - 1; i >= 0; i-- {
		err = a.shutdownFuncs[i](ctx)
		if err != nil {
			a.l.Sugar().Error(err)
		}
	}

	a.l.Info("app stopped")

	return nil
}
