CREATE TABLE comments 
(
    id bigint NOT NULL PRIMARY KEY, 
    owner_login VARCHAR(30) NOT NULL,
    type bigint NOT NULL,
    text TEXT NOT NULL,
    parent bigint
);
