package usecases

import (
	"comments/internal/ports"
	"fmt"

	"github.com/caarlos0/env"
)

type Comments struct {
	cfg *Config

	storage ports.Store
}

type Config struct {
}

func (comments *Comments) GetComments(parentId int) (string, error) {
	commentsJson, err := comments.storage.GetComments(parentId)
	if err != nil {
		return "", err
	}
	return commentsJson, nil
}

func (c *Config) Parse() error {
	if err := env.Parse(c); err != nil {
		return err
	}
	return nil
}

func New(storage ports.Store) (*Comments, error) {
	var cfg Config
	if err := env.Parse(&cfg); err != nil {
		return nil, fmt.Errorf("parse domain config failed: %w", err)
	}
	return &Comments{
		storage: storage,
	}, nil
}
