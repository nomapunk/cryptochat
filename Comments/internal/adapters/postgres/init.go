package postgres

import (
	"context"
	"fmt"

	"github.com/caarlos0/env"
)

type PostgresStorage struct {
	cfg Config
}

type Config struct {
	Postgres_url string `env:"POSTGRES_URL"`
}

func New() (*PostgresStorage, error) {
	var cfg Config
	if err := env.Parse(&cfg); err != nil {
		return nil, fmt.Errorf("parse postgres adapter configuration failed: %w", err)
	}
	return &PostgresStorage{
		cfg: cfg,
	}, nil
}

func (s *PostgresStorage) Stop(ctx context.Context) error {
	return nil
}
