package postgres

import (
	"comments/internal/domain/models"
	"context"
	"encoding/json"
	"fmt"

	"github.com/jackc/pgx/v4"
)

func (s *PostgresStorage) GetComments(parentId int) (string, error) {
	query := fmt.Sprintf("SELECT * FROM comments WHERE parent = %d;", parentId)
	db, err := pgx.Connect(context.Background(), s.cfg.Postgres_url)
	if err != nil {
		return "", err
	}
	defer db.Close(context.Background())
	rows, err := db.Query(context.Background(), query)
	if err != nil {
		return "", err
	}
	var comments []*models.Comment
	for rows.Next() {
		comment := new(models.Comment)
		err = rows.Scan(
			&comment.Id,
			&comment.OwnerLogin,
			&comment.CommentType,
			&comment.Text,
			&comment.Parent,
		)
		if err != nil {
			return "", err
		}
		comments = append(comments, comment)
	}
	jsonBytes, err := json.Marshal(comments)
	fmt.Println(string(jsonBytes), comments)
	if err != nil {
		return "", err
	}
	return string(jsonBytes), nil
}
