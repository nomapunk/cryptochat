package http

import (
	"io/ioutil"
	"net/http"

	"github.com/gin-gonic/gin"
)

func (a *Adapter) authMiddleWare(ctx *gin.Context) {
	// правда ли что создавать клиента каждый раз заебись идея?
	req, err := http.NewRequest("GET", a.cfg.AuthUrl, nil)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"text": err.Error(),
		})
		return
	}
	if accessToken, err := ctx.Cookie("access_token"); err == nil {
		req.AddCookie(&http.Cookie{Name: "access_token", Value: accessToken, Path: "/cryptochat"})
	}
	if refreshToken, err := ctx.Cookie("refresh_token"); err == nil {
		req.AddCookie(&http.Cookie{Name: "refresh_token", Value: refreshToken, Path: "/cryptochat"})
	}
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"text": err.Error(),
		})
		return
	}
	if resp.StatusCode != http.StatusOK {
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			ctx.JSON(http.StatusInternalServerError, gin.H{
				"text": err.Error(),
			})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"text": string(body),
		})
		return
	}
	cookie := resp.Cookies()
	for i := range cookie {
		ctx.Request.AddCookie(cookie[i])
	}
	ctx.Next()
}
