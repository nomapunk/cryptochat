package http

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func (a *Adapter) getComments(ctx *gin.Context) {
	parentIdQuery := ctx.Query("id")
	if len(parentIdQuery) == 0 {
		ctx.JSON(http.StatusInternalServerError, &gin.H{
			"text": "expected id",
		})
		return
	}
	parentId, err := strconv.Atoi(parentIdQuery)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, &gin.H{
			"text": err.Error(),
		})
		return
	}
	comments, err := a.comments.GetComments(parentId)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, &gin.H{
			"text": err.Error(),
		})
		return
	}
	ctx.JSON(http.StatusOK, comments)
}
