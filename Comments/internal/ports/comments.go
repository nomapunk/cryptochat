package ports

type Comments interface {
	GetComments(parentId int) (string, error)
}
