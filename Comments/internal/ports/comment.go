package ports

type Comment interface {
	GetId() int
	GetOwnerLogin() string
	GetType() int
	GetText() string
	GetParent() int
}
