package ports

type Store interface {
	GetComments(parentId int) (string, error)
}
